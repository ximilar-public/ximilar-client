import json
import sys
from argparse import ArgumentParser

from ximilar.client import (
    SimilarityPhotosClient,
    SimilarityProductsClient,
    SimilarityCustomClient,
    SimilarityFashionClient,
    ImageMatchingSearchClient,
    RestClient,
)
from ximilar.client.constants import RECORDS
from ximilar.client.utils.json_data import read_json_file_iterator, read_json_file_list


class Extractor:
    def __init__(self, token, extractor_url):
        # self.extractor_url = extractor_url
        self.client = RestClient(token, endpoint=extractor_url)

    def extract(self, args):
        # create batches of size args.batch_size from the input file
        batch = []
        counter = 0

        with open(args.output_file_path, "w") as f:
            for record in read_json_file_iterator(args.input_file_path, skip=args.skip):
                batch.append(record)
                counter += 1
                if len(batch) == args.batch_size:
                    self.extract_batch(batch, f)
                    batch = []
                    if counter >= args.limit:
                        break

    def extract_batch(self, records, output_fp):
        """
        Call extractor records into collection with all meta information.
        :param output_fp: file descriptor to write extracted records to
        :param records: dictionary with your "_id" and with one of "_url", "_file" or "_base64" to extract descriptor.
        :return: json response
        """
        print(f"Extracting {len(records)} records")
        data = {RECORDS: records}
        response = self.client.post("", data=data)
        for record in response[RECORDS]:
            if record["_status"]["code"] == 200:
                del record["_status"]
                json.dump(record, fp=output_fp)
                print("", file=output_fp)


if __name__ == "__main__":
    parser = ArgumentParser(description="Call extractor on records from a given file and write result to another")
    parser.add_argument("--extractor_url", help="extractor URL", required=True)
    parser.add_argument("--auth_token", help="user authorization token to be used for API authentication")
    parser.add_argument("--input_file_path", help="path to JSON file with image records", required=True)
    parser.add_argument("--output_file_path", help="path to JSON to print the result to", required=True)
    parser.add_argument("--batch_size", help="batch size for insert operation", default=10, type=int)
    parser.add_argument("--threads", help="# of threads to insert with", default=3, type=int)
    parser.add_argument("--skip", help="# of records from the file to be skipped", default=0, type=int)
    parser.add_argument("--limit", help="max number of records to be processed", type=int, default=None)

    args = parser.parse_args()

    extractor = Extractor(args.auth_token, args.extractor_url)
    extractor.extract(args)

    # if args.clean_fields:
    #     index_images = clean_fields(index_images, args.clean_fields)

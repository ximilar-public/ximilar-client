######################################################################################
#                              MIT License
# This script is able to process images of collectible cards/comics from the
# specified folder and return a CSV file with the extracted information.
#
# You will need to sign up to app.ximilar.com and have an API key to use this script.
# Your account must be at least on a Business plan to use the Collectibles API.
#
# Requirements and Installation:
#    You have to install python3 and requests library.
#       https://realpython.com/installing-python/
#
#       You can install requests & argparse library via pip system:
#            pip install --upgrade requests
#            pip install --upgrade argparse
#
# Example running use cases:
#   1. Process all images from folder 'data' with TCG_ID identification and CONDITION extraction
#   python process_card_scans.py --folder data --api_key APIKEY --collectible tcg --condition --output results.csv
#   2. Process all images from folder 'data/images' with SPORT identification
#   python process_card_scans.py --folder data/images --api_key APIKEY --collectible sport --output results.csv
######################################################################################

import argparse
import base64
import concurrent.futures
import csv
import os
import requests
import sys

from typing import Union
from pathlib import Path

CSV_DELIMITER = ";"
PRINT_PROGRESS_EVERY_NO_CARDS = 1
MAX_RETRIES = 1
SAVE_EVERY_N_CARDS = 10

ENDPOINTS = {
    "all": "https://api.ximilar.com/collectibles/v2/analyze",
    "tcg": "https://api.ximilar.com/collectibles/v2/tcg_id",
    "sport": "https://api.ximilar.com/collectibles/v2/sport_id",
    "slab": "https://api.ximilar.com/collectibles/v2/slab_id",
    "comics": "https://api.ximilar.com/collectibles/v2/comics_id",
    "grade": "https://api.ximilar.com/card-grader/v2/grade",
    "condition": "https://api.ximilar.com/card-grader/v2/condition",
}

SORT_IMAGES = lambda x: int(Path(x).stem.split("-")[-1]) if "-" in x else x.lower()

FILTER_IMAGES = {
    "all": lambda x: True,
    "even": lambda x: int(Path(x).stem.split("-")[-1]) % 2 == 0,
    "odd": lambda x: int(Path(x).stem.split("-")[-1]) % 2 != 0,
    "front": lambda x: "front" in x.lower(),
    "back": lambda x: "back" in x.lower(),
}

ENDPOINT_CONDITION = "https://api.ximilar.com/card-grader/v2/condition"
ENDPOINT_GRADE = "https://api.ximilar.com/card-grader/v2/grade"

SORT_FIELDNAMES = {
    "first": ["filename", "status", "side", "subcategory", "full_name", "name", "year", "card_number", "out_of"],
    "last": ["tcgplayer.com", "slab_grade", "slab_company", "condition_label", "condition_mode"],
}

ALLOWED_IMAGE_EXTENSIONS = ["jpg", "jpeg", "png", "bmp", "webp"]

ATTRS_TO_EXTRACT_ID = {
    "all": {"fields": ["name", "full_name", "year", "card_number", "set"]},
    "tcg": {
        "fields": [
            "name",
            "full_name",
            "year",
            "card_number",
            "subcategory",
            "set_code",
            "set",
        ],
        "tags": [
            {"Side": {"field": "side", "default": "front"}},
            {"Foil/Holo": {"field": "foil/holo", "default": "Non-Foil"}},
            {"Subcategory": {"field": "subcategory", "default": "UNKNOWN"}},
        ],
    },
    "sport": {
        "fields": [
            "name",
            "full_name",
            "year",
            "card_number",
            "sub_set",
            "subcategory",
            "team",
            "set_name",
            "company",
        ],
        "tags": [
            {"Side": {"field": "side", "default": "front"}},
            {"Foil/Holo": {"field": "foil/holo", "default": "Non-Foil"}},
            {"Subcategory": {"field": "subcategory", "default": "UNKNOWN"}},
        ],
    },
    "comics": {
        "fields": [
            "name",
            "title",
            "date",
            "publisher",
            "origin_date",
        ],
        "add": {"subcategory": "comics"},
    },
    "slab": {
        "fields": [
            "name",
            "brand",
            "grade",
            "certificate_number",
            "lang",
            "set",
        ],
        "add": {"subcategory": "slab"},
    },
}

ATTRS_TO_EXTRACT_ID_TCG_GAMES = {
    "Pokemon": {"fields": ["out_of", "series", "set_series_code"], "links": ["tcgplayer.com"]},
    "Digimon": {"fields": ["card_id", "series"]},
    "Dragon Ball Super": {"fields": ["card_id", "series"]},
    "Flesh and Blood": {"fields": ["card_id"]},
    "Star Wars Unlimited": {"fields": ["variation"]},
    "One Piece": {"fields": ["card_id"]},
    "Magic The Gathering": {"fields": [], "links": ["tcgplayer.com"]},
}

ATTRS_TO_EXTRACT_GRADE = [
    "edges",
    "corners",
    "surface",
    "centering",
    "final",
    "condition",
]

ATTRS_TO_EXTRACT_CONDITION = ["label", "mode"]


def print_progress_bar(iteration: int, total: int, suffix: str = "") -> None:
    """
    Print progress bar.
    :param suffix: string to add the end of the progress bar
    :param iteration: current iteration of the total
    :param total: number of iterations
    :return: None, only prints the progress bar
    """

    percent = ("{0:." + str(1) + "f}").format(100 * (iteration / float(total)))
    length = 30
    filled_length = int(length * iteration // total)
    bar = "█" * filled_length + "-" * (length - filled_length)

    # Use '\r' to return the cursor to the start of the line
    sys.stdout.write("\r")
    if suffix == "":
        sys.stdout.write(f"Progress |{bar}| {percent}%")
    else:
        sys.stdout.write(f"Progress |{bar}| {percent}%   {suffix}")

    sys.stdout.flush()  # Ensure the progress bar is displayed

    if iteration == total:
        print()  # Move to the next line after completing the progress


def retry(max_retries):
    """
    Retry decorator.

    :param max_retries:
    :return:
    """

    def decorator(func):
        def wrapper(*args, **kwargs):
            for i in range(max_retries):
                try:
                    return func(*args, **kwargs)
                except Exception as e:
                    import traceback

                    traceback.print_exc()
                    print(f"Attempt {i + 1} failed with error: {str(e)}")
            print(f"Function {func.__name__} failed after {max_retries} attempts")
            return None

        return wrapper

    return decorator


def load_base64_file(path: str) -> Union[str, None]:
    """
    Load file from disk to base64.

    :param path: local path to the image
    :return: base64 encoded string
    """
    try:
        with open(path, "rb") as image_file:
            return base64.b64encode(image_file.read()).decode("utf-8")
    except Exception:
        return None


def is_request_ok(resp: requests.Response) -> bool:
    """
    Check if the response from Ximilar API is OK.

    :param resp: response from Ximilar API
    :return: bool, true if is valid
    """

    if resp.status_code != 200:
        return False

    jresp: dict = resp.json()
    if len(jresp["records"]) != 1 or jresp["records"][0]["_status"]["code"] != 200:
        return False

    return True


def print_error(resp: requests.Response) -> None:
    """
    Print error message from the response.

    :param resp: response from Ximilar API
    :return: None
    """

    resp = resp.json()
    record = resp.get("records", [{}])[0]

    status_code = record.get("_status", {}).get("code", -1)
    text = record.get("_status", {}).get("text", "unknown")

    print(f"\nRequest failed with status code: {status_code}, text: {text}")


def batch(iterable, n=5):
    """
    Batch the iterable into chunks of size n.

    :param iterable: iterable to batch
    :param n: size of the chunks
    :return: generator of chunks
    """
    l = len(iterable)
    for ndx in range(0, l, n):
        yield iterable[ndx : min(ndx + n, l)]


def call_request(api_key: str, endpoint: str, data: dict) -> requests.Response:
    """
    Call the request to the Ximilar API.

    :param api_key: API key
    :param endpoint: endpoint of the API
    :param data: data for the request
    :return: response from the API
    """
    return requests.post(endpoint, headers=get_headers(api_key), json=data, timeout=60)


def get_headers(api_key: str) -> dict:
    """
    Get headers for the request.

    :param api_key: API key
    :return: headers
    """
    return {"Authorization": f"Token {api_key}", "Content-Type": "application/json"}


def get_data(card_image: str, slab_grade: bool = False) -> dict:
    """
    Get data for the request.

    :param card_image: base64 image of the card
    :return: data for the request
    """
    data: dict = {"records": [{"_base64": card_image}]}
    if slab_grade:
        data["slab_grade"] = True
    return data


def log_error(log_error_file: str, image_name: str, error_message: str) -> None:
    """
    Log the error message along with image name to the log file.

    :param log_error_file: path to the log file
    :param image_name: name of the image
    :param error_message: message to log
    :return: None
    """
    if log_error_file is None:
        return

    with open(log_error_file, "a") as f:
        f.write(f"image_name: {image_name}, e_message: {error_message}\n")


@retry(MAX_RETRIES)
def get_card_condition(api_key: str, card_image: str, card_obj: dict, log_error_file: str) -> bool:
    """
    Get card condition from Ximilar API and save the information to the card object.

    :param api_key:
    :param card_image: image in base64 of the card
    :param card_obj: object where to save the retrieved information
    :param log_error_file: path to the log file where errors will be saved
    :return: bool, true if the request was successful
    """
    resp = call_request(api_key, ENDPOINT_CONDITION, get_data(card_image))

    if not is_request_ok(resp):
        print_error(resp)
        log_error(
            log_error_file, card_obj["filename"], f"Getting condition failed, response from Ximilar service: {resp}"
        )
        return False

    resp = resp.json()
    record = resp["records"][0]
    if len(record["_objects"]) == 0:
        log_error(log_error_file, card_obj["filename"], "No objects found in the response during condition extraction")
        return False

    condition = record["_objects"][0].get("Condition", [])[0]
    if condition is None:
        log_error(
            log_error_file, card_obj["filename"], "No condition found in the response during condition extraction"
        )
        return False

    for attr_name in ATTRS_TO_EXTRACT_CONDITION:
        card_obj["condition_" + attr_name] = condition.get(attr_name)

    return True


@retry(MAX_RETRIES)
def get_card_grade(api_key: str, card_image: str, card_obj: dict, log_error_file: str) -> bool:
    """
    Get card grade from Ximilar API and save it to the card object.

    :param api_key:
    :param card_image: base64 image of the card
    :param card_obj: object where to save the retrieved information
    :param log_error_file: path to the log file where errors will be saved
    :return: bool, true if the request was successful
    """
    resp = call_request(api_key, ENDPOINT_GRADE, get_data(card_image))

    if not is_request_ok(resp):
        print_error(resp)
        log_error(log_error_file, card_obj["filename"], f"Grading failed, response from Ximilar service: {resp}")
        return False

    resp = resp.json()
    grades = resp["records"][0]["grades"]
    for attr_name in ATTRS_TO_EXTRACT_GRADE:
        card_obj["grade_" + attr_name] = grades.get(attr_name)

    return True


@retry(MAX_RETRIES)
def get_card_id(
    api_key: str,
    image: str,
    card_obj: dict,
    collectible_type: str,
    alternative: bool,
    slab_grade: bool,
    log_error_file: str,
) -> bool:
    """
    Identify the card using Ximilar API and save the information to the card object.

    :param api_key:
    :param image: base64 image of the card
    :param card_obj: object where to save the retrieved information
    :param collectible_type: type of card, i.e. sport, tcg, comics, slab…
    :param alternative: flag, determines if the alternative should be used
    :param slab_grade: flag, determines if the slab grade should be extracted
    :param log_error_file: path to the log file where errors will be saved
    :return: bool, true if the request was successful
    """

    def get_tag_value(tags, tag_name, default):
        if default in tags and len(tags[default]) > 0:
            return tags.get(default, [{"name": tag_name}])[0]["name"]
        return tag_name

    def pick_object(objects, collectible_type) -> Union[dict, None]:
        for obj in objects:
            if obj["name"] == "Card" and collectible_type in ["tcg", "sport"]:
                return obj
            elif obj["name"] == "Slab Label" and collectible_type == "slab":
                return obj
            elif obj["name"] == "Comic" and collectible_type == "comics":
                return obj
        return None

    resp = call_request(api_key, ENDPOINTS[collectible_type], get_data(image, slab_grade=slab_grade))

    if not is_request_ok(resp):
        print_error(resp)
        log_error(log_error_file, card_obj["filename"], f"Identification failed, response from Ximilar service: {resp}")
        return False

    record = resp.json()["records"][0]
    object = pick_object(record["_objects"], collectible_type) if len(record["_objects"]) > 0 else None
    slab_obj = pick_object(record["_objects"], "slab") if len(record["_objects"]) > 0 else None
    if object is None:
        log_error(
            log_error_file, card_obj["filename"], "No relevant object found in the response during identification"
        )
        return False

    # find the best matching item
    best_match = object.get("_identification", {}).get("best_match")
    alternatives = object.get("_identification", {}).get("alternatives", [])
    if not best_match and alternatives and alternative:  # if no best match, try the first alternative
        best_match = alternatives[0]
    elif not best_match and get_tag_value(object.get("_tags", {}), "Side", "front") == "front":
        log_error(
            log_error_file,
            card_obj["filename"],
            "No betch match, alternative or relevant tag found during identification",
        )
        return False

    # we want to extract the slab grade and company
    if slab_grade and slab_obj:
        card_obj["slab_company"] = get_tag_value(slab_obj.get("_tags", {}), None, "Company")
        card_obj["slab_grade"] = get_tag_value(slab_obj.get("_tags", {}), None, "Grade")

    # get the fields from the tags
    for tag in ATTRS_TO_EXTRACT_ID.get(collectible_type, {}).get("tags", {}):
        tag_name = list(tag.keys())[0]
        card_obj[tag[tag_name]["field"]] = get_tag_value(object.get("_tags", {}), tag[tag_name]["default"], tag_name)

    # get the attributes to extract
    attrs_to_extract = ATTRS_TO_EXTRACT_ID.get(collectible_type, {"fields": []})["fields"]
    if collectible_type in ["tcg", "sport"]:
        if best_match:
            subcategory = best_match.get("subcategory", "")
            attrs_to_extract = (
                attrs_to_extract + ATTRS_TO_EXTRACT_ID_TCG_GAMES.get(subcategory, {"fields": []})["fields"]
            )

            # get links to tcgplayer if possible
            links = ATTRS_TO_EXTRACT_ID_TCG_GAMES.get(subcategory, {"links": []}).get("links", [])
            for link in links:
                card_obj[link] = best_match.get("links", {}).get(link, None)

    # add additional attributes
    for attr_name in attrs_to_extract:
        if best_match:
            card_obj[attr_name] = best_match.get(attr_name)

    return True


def init_card(image_name: str, collectible: str, grade: bool, condition: bool) -> dict:
    """
    Initialize the card object with the image name with None values.

    :param collectible: type of collectible, i.e. sport, tcg, comics, slab…
    :param image_name: name of the file containing the image of the card
    :param tcg_id: flag, determines if the card should be identified first
    :param grade: flag, determines if the card should be graded
    :param condition: flag, determines if the card condition should be extracted
    :return: the card object
    """

    card: dict = {"filename": image_name}

    if collectible in ["tcg", "sport", "comics", "slab", "all"]:
        attrs_to_extract_ids = ATTRS_TO_EXTRACT_ID.get(collectible, [])
        for attr_name in attrs_to_extract_ids:
            card[attr_name] = None

    if grade:
        for attr_name in ATTRS_TO_EXTRACT_GRADE:
            card["grade_" + attr_name] = None

    if condition:
        for attr_name in ATTRS_TO_EXTRACT_CONDITION:
            card["condition_" + attr_name] = None

    return card


def call_apis(
    api_key, image_path, collectible, grade, condition, language, card_obj, alternative, slab_grade, log_error_file
):
    """
    Call all the APIs to save the information to the card object.

    :param api_key:
    :param image_path: path to the image of the card
    :param collectible: type of the cards, i.e. sport, tcg, comics, slab…
    :param tcg_id: flag, determines if the card should be identified first
    :param grade: flag, determines if the card should be graded
    :param condition: flag, determines if the card condition should be extracted
    :param language: determines the language of the response
    :param card_obj: object where to save the retrieved information
    :param alternative: flag, determines if the alternative should be used
    :param slab_grade: flag, determines if the slab grade should be extracted
    :param log_error_file: path to the log file where errors will be saved
    :return: card object with the retrieved information
    """
    try:
        if os.path.splitext(image_path)[1][1:].lower() not in ALLOWED_IMAGE_EXTENSIONS:
            card_obj["status"] = "invalid_format"
            log_error(log_error_file, card_obj["filename"], "Invalid format of image")
            return

        card_image = load_base64_file(image_path)

        if card_image is not None:
            if collectible in ["sport", "tcg", "comics", "slab"]:
                id_success = get_card_id(
                    api_key, card_image, card_obj, collectible, alternative, slab_grade, log_error_file
                )
                if not id_success:
                    raise Exception("Identification failed: %s" % image_path)

            if grade or collectible in ["grade"]:
                grade_success = get_card_grade(api_key, card_image, card_obj, log_error_file)
                if not grade_success:
                    raise Exception("Grading failed: %s" % image_path)

            if condition or collectible in ["condition"]:
                condition_success = get_card_condition(api_key, card_image, card_obj, log_error_file)
                if not condition_success:
                    raise Exception("Getting condition failed: %s" % image_path)

        card_obj["status"] = "ok"
    except Exception as e:
        card_obj["status"] = "error"
    return card_obj


def process(
    api_key: str,
    process_error: bool,
    folder: str,
    output_path: str,
    collectible: str,
    grade: bool,
    condition: bool,
    language: bool,
    alternative: bool,
    slab_grade: bool,
    log_error_file: str,
    workers: int = 1,
    select_images: str = "all",
):
    """
    Process the images in the folder and save the information to the output CSV file.

    :param api_key: Ximilar API key from app.ximilar.com/settings
    :param process_error: flag, determines if the error rows should be processed
    :param folder: folder name that contains the images
    :param output_path: path to the output CSV
    :param collectible: type of the cards, i.e. sport, tcg, comics, slab…
    :param grade: flag, determines if the card should be graded
    :param condition: flag, determines if the card condition should be extracted
    :param language: determines the language of the response
    :param alternative: flag, determines if the alternative should be used
    :param slab_grade: flag, determines if the slab grade should be extracted
    :param log_error_file: path to the log file where errors will be saved
    :param workers: number of workers for multi-threading
    :param select_images: how to select the images
    :return: None
    """
    print("Processing...")

    dir_content = os.listdir(folder)
    dir_content = sorted(dir_content, key=SORT_IMAGES)
    dir_content = list(filter(FILTER_IMAGES[select_images], dir_content))

    with concurrent.futures.ThreadPoolExecutor(max_workers=workers) as executor:
        progress_counter = 0
        card_objs = {}
        successes = 0

        read_output(output_path, card_objs)

        for images in batch(dir_content, n=5):
            futures = []
            for image_name in images:
                if image_name in card_objs and (card_objs[image_name]["status"] == "ok" or not process_error):
                    successes += 1
                    progress_counter += 1
                    print_progress_bar(progress_counter, len(dir_content), f"filename: {image_name}")
                    continue

                card_obj = init_card(image_name, collectible, grade, condition)
                card_objs[card_obj["filename"]] = card_obj

                futures.append(
                    executor.submit(
                        call_apis,
                        api_key,
                        os.path.join(folder, image_name),
                        collectible,
                        grade,
                        condition,
                        language,
                        card_obj,
                        alternative,
                        slab_grade,
                        log_error_file,
                    )
                )

            # for for printing the progress and saving the output
            for future in concurrent.futures.as_completed(futures):
                res = future.result(timeout=100)
                processed_filename = ""

                if res:
                    processed_filename = f"filename: {res.get('filename', '')}"
                    successes += 1

                progress_counter += 1
                if progress_counter % PRINT_PROGRESS_EVERY_NO_CARDS == 0:
                    print_progress_bar(progress_counter, len(dir_content), processed_filename)

                if progress_counter % SAVE_EVERY_N_CARDS == 0:
                    write_output(output_path, card_objs)

    write_output(output_path, card_objs)
    print(f"Processing finished!")
    print(f"Successfully processed {successes} out of {len(dir_content)} cards")
    print(f"Output saved to {output_path}")


def write_output(output_path: str, card_objs: dict) -> None:
    """
    Write the card information to the output CSV file.

    :param output_path: path where to save the output CSV file
    :param card_objs: list of card objects
    :param select_images: sorting of the images
    :return:
    """
    with open(output_path, "w", newline="") as csvfile:
        fieldnames = set()
        for obj in card_objs.values():
            for key in obj.keys():
                fieldnames.add(key)

        fieldnames = sorted(list(fieldnames))

        for i, fieldname in enumerate(SORT_FIELDNAMES["first"]):
            if fieldname in fieldnames:
                fieldnames.pop(fieldnames.index(fieldname))
                fieldnames.insert(i, fieldname)

        for i, fieldname in enumerate(SORT_FIELDNAMES["last"]):
            if fieldname in fieldnames:
                fieldnames.pop(fieldnames.index(fieldname))
                fieldnames.append(fieldname)

        writer = csv.DictWriter(csvfile, fieldnames=fieldnames, delimiter=CSV_DELIMITER)

        writer.writeheader()
        for filename in sorted(card_objs.keys(), key=SORT_IMAGES):
            writer.writerow(card_objs[filename])


def read_output(output_path: str, card_objs: dict) -> None:
    """
    Read the output CSV file and load the card objects.

    :param output_path: path to the output CSV file
    :param card_objs: list where to save the card objects
    """
    if not os.path.exists(output_path):
        return

    with open(output_path, "r") as csvfile:
        reader = csv.DictReader(csvfile, delimiter=CSV_DELIMITER)
        for row in reader:
            card_objs[row["filename"]] = row


def main():
    parser = argparse.ArgumentParser(description="Parse input arguments")
    parser.add_argument(
        "--collectible",
        choices=["comics", "tcg", "sport", "slab", "all", "grade", "condition"],
        help="Type of collectible",
        required=True,
    )
    parser.add_argument(
        "--api_key",
        help="Ximilar API key (from app.ximilar.com/settings)",
        required=False,
        default=os.environ.get("XIMILAR_API_KEY"),
    )
    parser.add_argument(
        "--log_error_file",
        help="Path to the log file where errors will be saved. + \
                        When not set, the errors will not be logged",
        default="erorrs.log",
    )
    parser.add_argument("--folder", help="Path to folder where are images located", required=True)
    parser.add_argument("--output", nargs="?", default="output.csv", help="Output file name")
    parser.add_argument(
        "--process_error", action="store_true", default=False, help="If set true, the error rows will be processed"
    )
    parser.add_argument("--grade", action="store_true", default=False, help="If set true, the grade will be extracted")
    parser.add_argument(
        "--condition", action="store_true", default=False, help="If set true, the condition will be extracted"
    )
    parser.add_argument("--max_retries", type=int, default=1, help="Number of retries")
    parser.add_argument(
        "--alternative", action="store_true", default=False, help="If not best match is found, try alternative"
    )
    parser.add_argument(
        "--slab_grade",
        action="store_true",
        default=False,
        help="If set true, the slab grade and company will be extracted",
    )
    parser.add_argument("--language", action="store_true", default=False, help="Language")
    parser.add_argument("--num_workers", type=int, default=1, help="Number of workers")
    parser.add_argument(
        "--select_images",
        choices=["all", "even", "odd", "front", "back"],
        help="""
            How to pick images for processing 
                all - process everyting from the folder,
                even - only filenames with even numbers at the end,
                odd - only filenames with odd numbers at the end,
                front - only filenames with front in the name,
                back - only filenames with back in the name.
            )
        """,
        default="all",
        required=True,
    )
    args = parser.parse_args()
    MAX_RETRIES = args.max_retries

    if not args.api_key:
        print(
            "API key is missing, please provide it via --api_key argument. \n"
            "You can get API key from app.ximilar.com/settings. \n"
            "Be aware that your account must be at least on a Business plan to use the Collectibles API."
        )
        return

    if not os.path.exists(args.folder) or not os.path.isdir(args.folder):
        print("Folder with image cards does not exist or is a directory")
        return

    process(
        args.api_key,
        args.process_error,
        args.folder,
        args.output,
        args.collectible,
        args.grade,
        args.condition,
        args.language,
        args.alternative,
        args.slab_grade,
        args.log_error_file,
        args.num_workers,
        args.select_images,
    )


if __name__ == "__main__":
    main()
